<!--- vim: set et sw=2 ts=2 tw=80 : -->

# cordova-plugin-argo-api

Cordova plugin to read marks from the italian online e-school register
*Argo Scuolanext*.

## Abandoned

This code was needed to build *OpenVoti*, an Ionic 3 client for *Argo*. So, the
project is currently unmantained.
