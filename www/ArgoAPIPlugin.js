/*
 * cordova-plugin-argo-api - Cordova plugin for reading marks from the eDiary Argo Scuolanext
 * Copyright (C) 2017  Claudio Maggioni
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

var exec = require("cordova/exec");

exports.getMarks = function(user) {
    return new Promise(function(successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "ArgoAPIPlugin", "getMarks", [user]);
    });
};