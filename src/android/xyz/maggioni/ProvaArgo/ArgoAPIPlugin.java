/*
 * cordova-plugin-argo-api - Cordova plugin for reading marks from the eDiary Argo Scuolanext
 * Copyright (C) 2017  Claudio Maggioni
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.maggioni.ProvaArgo;

import org.apache.cordova.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.nodes.Element;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ArgoAPIPlugin extends CordovaPlugin {

    public static void main(String... args) {
        PluginLOG.useCordova = false;

        JSONArray arr = new JSONArray();
        JSONObject obj = new JSONObject();
        obj.put("username", "maggioni");
        obj.put("password", "huuu273j");
        obj.put("schoolCode", "SG26426");
        arr.put(obj);

        ArgoAPIPlugin a = new ArgoAPIPlugin();

        try {
            System.out.println(a.getMarks(arr).toString(2));
        } catch (ArgoException | JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean execute(String action, final JSONArray args, final CallbackContext callbackContext) throws JSONException {
        if (action.equals("getMarks")) {

            final ArgoAPIPlugin this__ = this;

            new Thread(() -> {
                try {
                    this__.getMarks(args);
                } catch (ArgoException e) {
                    callbackContext.error(e.getResponse());
                } catch (JSONException e1) {
                    callbackContext.error(e1.getLocalizedMessage());
                }
            }).start();

            return true;
        }
        return false;
    }

    private JSONObject getMarks(final JSONArray args) throws JSONException, ArgoException {
        return ArgoAPI.fetchDocument(args, ArgoAPI.ArgoDocument.MARKS, document -> {
            JSONObject toReturn = new JSONObject();
            JSONArray subjects = new JSONArray();
            toReturn.put("success", true);
            toReturn.put("subjects", subjects);
            toReturn.put("message", "Success");

            Element e = document.getElementById("sheet-sezioneDidargo:panel-votiGiornalieri:pannello");

            for (Element el : e.select("fieldset")) {

                JSONObject subject = new JSONObject();
                JSONArray marks = new JSONArray();
                subject.put("name", el.select("legend").first().text());
                subject.put("marks", marks);

                String lastDate = null;
                for (Element el2 : el.select("table > tbody > tr")) {
                    JSONObject mark = new JSONObject();

                    Pattern p = Pattern.compile("\\d\\d\\d\\d-\\d\\d-\\d\\d");
                    Matcher m2 = p.matcher(el2.text());
                    if (!m2.find()) {
                        if (lastDate == null) throw new IllegalStateException("Date not found in: \"" + el2.text() + "\"");
                        // if date has not been written, assume the date is equal to the last known date
                        // (already in last date)
                    } else {
                        lastDate = m2.group(0);
                    }
                    mark.put("date", new SimpleDateFormat("yyyy-MM-dd").parse(lastDate));

                    Pattern regMark = Pattern.compile("((?:[Ss]critto)|(?:[Oo]rale)|(?:[Pp]ratico)).*\\(([0-9.]+)\\)");
                    Matcher m = regMark.matcher(el2.text());
                    if (!m.find()) throw new IllegalStateException("Mark not found in: \"" + mark + "\"");
                    mark.put("type", m.group(1));
                    mark.put("value", Double.parseDouble(m.group(2)));

                    marks.put(mark);
                }

                subjects.put(subject);
            }

            return toReturn;
        });
    }
}

