/*
 * cordova-plugin-argo-api - Cordova plugin for reading marks from the eDiary Argo Scuolanext
 * Copyright (C) 2017  Claudio Maggioni
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.maggioni.ProvaArgo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection.*;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

class ArgoAPI {
    private ArgoAPI() {
    }
    
    private static void logResponse(Response res) {
        PluginLOG.i("ArgoAPI", "Response URL: " + res.url());
        PluginLOG.i("ArgoAPI", "Response code: " + res.statusCode());
        PluginLOG.i("ArgoAPI", "Response status: " + res.statusMessage());

        for (Map.Entry<String, String> entry : res.cookies().entrySet()) {
            PluginLOG.i("ArgoAPI", "Response cookie: " + entry.getKey() + "=" + entry.getValue());
        }
    }

    private static void updateCookies(Map<String, String> old, Map<String, String> returned) {
        for (Map.Entry<String, String> entry : returned.entrySet()) {
            old.put(entry.getKey(), entry.getValue());
        }
    }

    private static boolean login(Map<String, String> cookies, User user) {
        try {
            Response initRes = Jsoup.connect("https://www.portaleargo.it/argoweb/famiglia/?cod_utente=" + user.getSchoolCode())
                    .followRedirects(true)
                    .method(Connection.Method.GET)
                    .execute();

            updateCookies(cookies, initRes.cookies());
            logResponse(initRes);

            PluginLOG.i("ArgoAPI", "Cookie init complete");

            Map<String, String> params = new HashMap<>();
            params.put("utente", user.getUsername());
            params.put("j_password", user.getPassword());
            params.put("j_username", user.getUsername() + "#" + user.getSchoolCode());
            params.put("submit", "Entra");

            Response loginRes = Jsoup.connect("https://www.portaleargo.it/argoweb/famiglia/common/j_security_check")
                    .cookies(cookies)
                    .referrer("https://www.portaleargo.it/argoweb/famiglia/common/login_form2.jsp")
                    .data(params)
                    .method(Connection.Method.POST)
                    .followRedirects(true)
                    .execute();

            updateCookies(cookies, loginRes.cookies());
            logResponse(loginRes);

            PluginLOG.i("ArgoAPI", "login complete");
            Document d = loginRes.parse();

            return !d.select("html > head > title").isEmpty()
                    && "Argo - Famiglia".equalsIgnoreCase(d.select("html > head > title").first().text());

        } catch (IOException e) {
            PluginLOG.e("ArgoAPI", e.getLocalizedMessage(), e);
            return false;
        }
    }

    enum ArgoDocument {
        MARKS("[evt=menu-servizialunno:vot|event|custom]"),
        HOMEWORK("[evt=menu-serviziclasse:_idJsp25|event|submit]"),
        LESSONS("[evt=menu-serviziclasse:_idJsp27|event|submit]"),
        LOGOUT("[evt=_idJsp66|event|custom|eventSrc|Uscire%20dal%20programma%3F|param1||param2||param3||param4|]");

        private String backbaseDelta;
        ArgoDocument(String s) {
            backbaseDelta = s;
        }
    }

    interface DocumentParser {
        JSONObject parseDocument(Document document) throws Exception;
    }

    private static Document fetchHTML(ArgoDocument document, Map<String, String> cookies) throws IOException {
        try {
            Response markRes = Jsoup.connect("https://www.portaleargo.it/argoweb/famiglia/index.jsf")
                    .cookies(cookies)
                    .referrer("https://www.portaleargo.it/argoweb/famiglia/index.jsf")
                    .data("BackbaseClientDelta", document.backbaseDelta)
                    .followRedirects(true)
                    .method(Connection.Method.POST)
                    .execute();

            updateCookies(cookies, markRes.cookies());
            logResponse(markRes);
            return Jsoup.parse(markRes.body().replaceAll("Â", ""));
        } catch (IOException e) {
            PluginLOG.e("ArgoAPI", e.getLocalizedMessage(), e);
            throw e;
        }
    }

    public static JSONObject fetchDocument(JSONArray args, ArgoDocument document, DocumentParser parser)
            throws JSONException, ArgoException {
        JSONObject response;

        try {
            JSONObject jsonObject = args.getJSONObject(0);

            // parse user credentials
            User user = new User(jsonObject.getString("username"),
                    jsonObject.getString("password"),
                    jsonObject.getString("schoolCode"));

            Map<String, String> cookies = new HashMap<>();

            // login
            if (!ArgoAPI.login(cookies, user)) {
                throw new IllegalStateException("Login on Argo failed");
            }

            // fetch the HTML page
            Document html = fetchHTML(document, cookies);

            // parse the HTML into JSON
            response = parser.parseDocument(html);

            // log out
            fetchHTML(ArgoDocument.LOGOUT, cookies);

        } catch (Exception e) {
            PluginLOG.e("ArgoAPI", e.getMessage(), e);
            try {
                response = new JSONObject();
                response.put("success", false);
                response.put("subjects", new JSONArray());
                response.put("message", e.getLocalizedMessage());
            } catch (JSONException e1) {
                PluginLOG.e("ArgoAPI", e1.getMessage(), e1);
                throw e1;
            }
        }

        try {
            if (response.get("success").equals(true)) {
                return response;
            } else {
                throw new ArgoException(response);
            }
        } catch (JSONException | ArgoException e) {
            PluginLOG.e("ArgoAPI", e.getMessage(), e);
            throw e;
        }
    }
}
