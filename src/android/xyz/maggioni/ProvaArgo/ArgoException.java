/*
 * cordova-plugin-argo-api - Cordova plugin for reading marks from the eDiary Argo Scuolanext
 * Copyright (C) 2017  Claudio Maggioni
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.maggioni.ProvaArgo;

import org.json.JSONObject;

public class ArgoException extends Exception {
    private JSONObject response;

    ArgoException(JSONObject response) {
        this.response = response;
    }

    public JSONObject getResponse() {
        return response;
    }

    @Override
    public String getMessage() {
        return "Marks fetch failed with the following data: "+response.toString();
    }
}
