/*
 * cordova-plugin-argo-api - Cordova plugin for reading marks from the eDiary Argo Scuolanext
 * Copyright (C) 2017  Claudio Maggioni
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.maggioni.ProvaArgo;

import org.apache.cordova.LOG;

public class PluginLOG {
    public static boolean useCordova = true;

    public static void i(String tag, String message) {
        if (useCordova) LOG.i(tag, message);
        else System.out.println("INFO (" + tag + "): " + message);
    }

    public static void e(String tag, String s, Throwable e) {
        if (useCordova) LOG.e(tag, s, e);
        else {
            System.err.println("ERROR (" + tag + "): " + s);
            e.printStackTrace(System.err);
        }
    }
}
