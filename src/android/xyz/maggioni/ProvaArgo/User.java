/*
 * cordova-plugin-argo-api - Cordova plugin for reading marks from the eDiary Argo Scuolanext
 * Copyright (C) 2017  Claudio Maggioni
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package xyz.maggioni.ProvaArgo;

public class User {
    private final String password;
    private final String schoolCode;
    private final String username;

    User(String username, String password, String schoolCode) {
        this.username = username;
        this.password = password;
        this.schoolCode = schoolCode;
    }

    String getUsername() {
        return username;
    }

    String getPassword() {
        return password;
    }

    String getSchoolCode() {
        return this.schoolCode;
    }

    @Override
    public String toString() {
        return "User{" +
                "password='" + password + '\'' +
                ", schoolCode='" + schoolCode + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}